# DIPMP_exercises

Exercises of the course "Digital Image Processing in Medical Physics"

## Description
The project contains the Python scripts and the input data for the exercises of the course "Digital Image Processing in Medical Physics". Each of the exercises is contained in a specific folder, with the name corresponding to the assigned exercise sheet. A test folder and a tutorial on digital image processing are also provided.

## Connection
You can connect your machine to this GitLab repository and easily clone the code (described below for Linux). Alternatively you can simply download the .zip file


1. If you wish to use Linux, from a Windows or Mac, you can connect to the servers in the LMU physics CIP pool:
https://collab.dvb.bayern/display/LMULMPHGST/CIP+Pools#CIPPools-RemoteAccess


2. You will need to connect to the VPN using the EduVPN client: https://www.eduvpn.org/


The configuration to connect via EduVPN is explained here:

https://doku.lrz.de/pages/viewpage.action?pageId=87425051&showLanguage=en_GB


From Windows, one can connect to the CIP pool workstations using the remote desktop application.

The list of the available workstations is available here:
https://collab.dvb.bayern/display/LMULMPHGST/Details+on+Available+Resources#DetailsonAvailableResources-NewAlmaLinux9Workstations


## Connect to the GitLab server
Login to your gitlab account with your LMU identification:
https://gitlab.physik.uni-muenchen.de/

(see https://www.en.it.physik.uni-muenchen.de/dienste/datenspeicherung/gitlab/index.html)


## Clone the repository
1. On your machine, set up a directory for the exercises, e.g.:

	mkdir /path/to/exercises_folder/

2. Within this directory, clone the gitlab repository with:

	git clone 	https://gitlab.physik.uni-muenchen.de/Marco.Riboldi/dipmp_exercises.git

3. You can also copy the repository by downloading a compressed file at the following public address:

https://gitlab.physik.uni-muenchen.de/Marco.Riboldi/dipmp_exercises


## Computational framework
The code will be presented on the PyCharm IDE:
https://www.jetbrains.com/pycharm/

1. On the CIP servers, PyCharm is installed. In the terminal, you can launch it via the following commands:

```
module load pycharm
pycharm.sh

```


2. Local installation on your PC:

https://www.jetbrains.com/help/pycharm/python.html


## Create your project

Create your project using the following steps:

1. File ->  New Project

Location: select folder (directory for the exercises)

2. Select New environment using: Virtualenv (set python 3.8 as base interpreter)

Location: select folder/venv

3. Then press Create

4. Warning that Directory is not empty: -> Create from existing sources

## Virtual environment

The file requirements.txt lists all necessary python libraries and their versions used throughout the exercises

We recommend installing them within a virtual environment 

https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#env-requirements

Once the project has been created, opening any .py file or requirements.txt prompts to the installation of the necessary packages

Click -> install requirements

The required packages can also be installed from the terminal:

	pip install -r requirements.txt


If prompted upgrade your pip installation: 

	pip install --upgrade pip

## Test run
Run test.py in the "test" folder. Type: 

	python \test\test.py

In the PyCharm terminal (assuming you are in the main repository folder.

Alternatively, you need to open the file test.py and define the run/debug configuration:

1. Define a name for your configuration 

2. Select the virtual environment as the Interpreter

3. Complete the script path with the same path where you have the script test.py

4. You should see a CT image loaded and displayed in an output window

